module.exports = function (grunt) {

    grunt.initConfig({
        watch: {
            options: {
                dateFormat: function (time) {
                    grunt.log.writeln('The watch finished in ' + time + 'ms at' + (new Date()).toString());
                    grunt.log.writeln('Waiting for more changes...');
                }
            },
            dist: {
                files: [
                    // 'js/**/*',
                    'scss/**/*.scss',
                    // 'vendor/**/*'
                ],
                tasks: ['sass']
            }
        },
        sassdoc: {
            default: {
                src: 'scss/*.scss',
                options: {
                    dest: 'sassdoc/',
                    display: {
                        access: ['public', 'private'],
                        alias: true,
                        watermark: true,
                    },
                    groups: {
                        slug: 'Title',
                        helpers: 'Helpers',
                        hacks: 'Dirty Hacks & Fixes',
                        'undefined': 'Ungrouped',
                    },
                    basePath: 'https://github.com/SassDoc/grunt-sassdoc',
                },
            },
        },
        sass: {                              // Task
            dist: {                            // Target
                options: {                       // Target options
                    style: 'expanded'
                },
                files: {                         // Dictionary of files
                    'dist/teste.css': 'scss/style.scss'
                }
            }
        }

    });
    // Plugins do Grunt
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sassdoc');

    // Tarefas que serão executadas
    grunt.registerTask('default', ['sass', 'cssmin']);
    grunt.registerTask('build', ['watch']);
};